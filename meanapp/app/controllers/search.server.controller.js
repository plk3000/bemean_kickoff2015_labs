'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	http = require('http'),
	url = require('url');

exports.search = function(req, res){
	var post_options = url.parse('http://192.168.50.10:9200/people/_search');
	post_options.method = 'POST';
	post_options.headers = {
		'Content-Type' : 'application/json'
	};
	var httpRequest = http.request(post_options, function(response){ // maybe use https://www.npmjs.com/package/request instead of http
		/*
		Process the ES response and send it back 'response' is the response from ES 'res' is what you use to send a response to the client
		 */
	}).on('error', function(error){
		res.status(500).jsonp(error);
	});

	httpRequest.write(JSON.stringify(req.body));
	httpRequest.end();
};
