'use strict';

// People search module config
angular.module('people-search').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Search', 'search', 'item', '/search');
	}
]);
