'use strict';

angular.module('people-search').controller('SearchController', ['$scope', 'SearchAPI', 'Search',
	function ($scope, SearchAPI, Search) {

		$scope.query = '*';
		var queryPart = {
			'query_string': {
				'query': '*'
			}
		};

		var aggregationsPart = {
			'age': {
				'range': {
					'field': 'age',
					'ranges': [
						{
							'from': 20,
							'to': 25
						},
						{
							'from': 25,
							'to': 30
						},
						{
							'from': 30,
							'to': 35
						},
						{
							'from': 35
						}
					]
				}
			},
			'company': {
				'terms': {
					'field': 'company',
					'size': 5
				}
			},
			'tags': {
				'terms': {
					'field': 'tags',
					'size': 5
				}
			},
			'favoriteFruit': {
				'terms': {
					'field': 'favoriteFruit',
					'size': 5
				}
			}
		};

		var filterPart = {};
		$scope.filters = [];

		$scope.addFilter = function(filter, field){
			/*
			Add the field to the root of the filter then
			add the filter to the $scope.filters and get the ES filter from the Search service
			and execute the search
			 */
		};

		$scope.removeFilter = function(filter){
			/*
			 Remove the clicked filter and update the results
			 */
		};

		$scope.doSearch = function () {
			/*
			Construct the search request body and execute the call to ES using the SearchAPI Service
			 */
		};

		$scope.doSearch();
	}
]);
