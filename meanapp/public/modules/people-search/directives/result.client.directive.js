'use strict';

angular.module('people-search').directive('result', [
	function() {
		return {
			templateUrl: 'modules/people-search/views/directives/result.client.view.html',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
			}
		};
	}
]);
