'use strict';

// Todos controller
angular.module('todos').controller('TodosController', ['$scope', '$stateParams', '$location', 'Authentication', 'Todos',
	function($scope, $stateParams, $location, Authentication, Todos) {
		$scope.authentication = Authentication;
		$scope.datePickerOpened = false;
		
		$scope.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1
		  };
		  
		  $scope.disabled = function(date, mode) {
		    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		  };
		
		$scope.openDatePicker = function(){
			
			console.log($scope.datePickerOpened);
			$scope.datePickerOpened = !$scope.datePickerOpened;
			console.log($scope.datePickerOpened);
		};

		$scope.save = function($event){
			if ($event.which === 13 && $scope.content) {
				$scope.create();
			}
		};

		// Create new Todo
		$scope.create = function() {
			// Create new Todo object
			var todo = new Todos ({
				content: this.content
			});
			$scope.content = '';

			// Redirect after save
			todo.$save(function(response) {
				$scope.find();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Todo
		$scope.remove = function(todo) {
			if ( todo ) { 
				todo.$remove();

				for (var i in $scope.todos) {
					if ($scope.todos [i] === todo) {
						$scope.todos.splice(i, 1);
					}
				}
			} else {
				$scope.todo.$remove(function() {
					$location.path('todos');
				});
			}
		};

		// Update existing Todo
		$scope.update = function(todo) {
			if(!todo){
				todo = $scope.todo;
			}
			todo.$update(function() {
				$scope.find();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Todos
		$scope.find = function() {
			Todos.query({isCompleted: false}).$promise.then(function(data){
				$scope.incompleteTodos = data;
			});
			Todos.query({isCompleted: true}).$promise.then(function(data){
				$scope.completedTodos = data;
			});
		};

		// Find existing Todo
		$scope.findOne = function() {
			$scope.todo = Todos.get({ 
				todoId: $stateParams.todoId
			});
		};
	}
]);